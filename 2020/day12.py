import numpy as np
import copy as copy

def TraversePart1(data):

    traversalLog = {"N": 0, "E": 0, "S": 0, "W": 0}
    compass = ["N", "E", "S", "W"]
    compassPointer = 1
    currentDirection = compass[compassPointer]

    for action, value in data:

        if (action == "F"):
            traversalLog[currentDirection] += value

        elif (action == "L"):
            rotation = value//90
            compassPointer -= rotation + 4
            compassPointer -= 4*(compassPointer//4)
            currentDirection = compass[compassPointer]

        elif (action == "R"):            
            rotation = value//90
            compassPointer += rotation
            compassPointer -= 4*(compassPointer//4)
            currentDirection = compass[compassPointer]

        else:
            traversalLog[action] += value

    manhattanDist = (abs(traversalLog["N"] - traversalLog["S"]) 
                     + abs(traversalLog["E"] - traversalLog["W"]))
    return manhattanDist

def Rotation2D(vector,
               angle:int, 
               centerOfRotation=np.array([0, 0])):
    
    radian = (np.pi/2) * (angle//90)
    cos = int(np.cos(radian))
    sin = int(np.sin(radian))
    R = np.array([[cos, -sin],
                  [sin, cos]], dtype=int)

    # rotate
    vector = np.matmul(R, vector)

    return vector


def TraversePart2(data):
    ship = np.array([0, 0], dtype=int)
    waypoint = np.array([1, 10], dtype=int)

    step = 0
    for action, value in data:
        # print("step =", step, ", action =", action, ", value =", value)

        if (action == "N"):
            waypoint[0] += value
        
        elif (action == "S"):
            waypoint[0] -= value

        elif (action == "E"):
            waypoint[1] += value
        
        elif (action == "W"):
            waypoint[1] -= value

        elif (action == "F"):
            ship[0] += waypoint[0] * value
            ship[1] += waypoint[1] * value
            
        elif (action == "L"):
            waypoint = Rotation2D(vector=waypoint, 
                                  angle=-value, 
                                  centerOfRotation=ship)
        elif (action == "R"):
            waypoint = Rotation2D(vector=waypoint,
                                  angle=value,
                                  centerOfRotation=ship)
            
    manhattanDistance = abs(ship[0]) + abs(ship[1])

    return manhattanDistance


# load data
path = "data/data12.txt"
with open(path,"r") as file:
    data = [[row[0], int(row[1:].rstrip())] for row in file]
file.close()

# part 1
part1ManhattanDistance = TraversePart1(data)
# part 2
part2ManhattanDistance = TraversePart2(data)



print("\nDay 12 : Rain Risk\n")
print()
print("--- part 1 ---")
print("manhattan distance after traversal =", part1ManhattanDistance)
print()
print("--- part 2 ---")
print("manhattan distance after traversal =", part2ManhattanDistance)
print()