import numpy as np
import lib.utils as utils


# load line data
slopeLines = np.genfromtxt("data/data03.txt",dtype=str,comments=None)
slopeArray = utils.SlopeArrayGenerator(slopeLines)

# --- part 1 ---
hStart = 0
hStep = 3
vStep = 1
treesOnSlope31 = utils.SlopeRiderTreeCounter(slopeArray,hStart,hStep,vStep)

print("\nDAY 03 : Toboggan Trajectory\n")
print("--- part 1 ---")
print("trees with vStep=1 and hStep=3 =",treesOnSlope31)
print()

vStepArray = np.array([1,1,1,1,2])
hStepArray = np.array([1,3,5,7,1])
treeProduct = 1
for i in range(0,len(vStepArray)):
    treeProduct = treeProduct*utils.SlopeRiderTreeCounter(slopeArray,hStart,hStep=hStepArray[i],vStep=vStepArray[i])

print("--- part 2 ---")
print("tree product from all slope lines =",treeProduct)
print()