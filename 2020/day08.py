import numpy as np
from lib.utils import *

# import puzzle input
filepath = "data/data08.txt"

bootCodeClass = BootCode(filename=filepath)
bootCodeClass.Load()
bootCodeClass.RunBootCode()
failedAccumulatorValue = bootCodeClass.failedAccumulatorValue
bootCodeClass.FixBootCode()
successAccumulatorValue = bootCodeClass.successAccumulatorValue


print("\nDay 08 : handheld Halting\n")
print()
print("--- part 1 ---")
print("accumulator value at failure =", failedAccumulatorValue)
print()
print("--- part 2 ---")
print("accumulator value after debug =", successAccumulatorValue)
print()
