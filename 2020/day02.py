import numpy as np
from lib.utils import CheckPasswordSled, CheckPasswordToboggan


# load data
passwordsArray = np.genfromtxt("data/data02.txt", dtype=str, delimiter="\n")

# check password useability
useablePasswordsSled = 0
useablePasswordsToboggan = 0
for password in passwordsArray:
    useablePasswordsSled += CheckPasswordSled(password)
    useablePasswordsToboggan += CheckPasswordToboggan(password)

print("\nDAY 02 : Password Philosophy\n")
print("--- part 1 ---")
print("useable passwords (Sled method) =", useablePasswordsSled)
print()

print("--- part 2 ---")
print("useable passwords (Toboggan method) =", useablePasswordsToboggan)
print()