from lib.utils import DecoderXMAS

filename = "data/data09.txt"

Decoder = DecoderXMAS(filename=filename)
Decoder.Load()
# Decoder.CheckData()
Decoder.FindContigousSet()

nonConforming = Decoder.nonComformingValue
constigousSet = Decoder.contigousSet
weaknessSum = min(constigousSet) + max(constigousSet)


print("\nDay 09 : Encoding Error\n")
print()
print("--- part 1 ---")
print("nonconforming value =", Decoder.nonComformingValue)
print()
print("--- part 2 ---")
print("sum of first and last value in contigous set =", weaknessSum)
print()