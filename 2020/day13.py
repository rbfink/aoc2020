import numpy as np


def LoadData(path:str):
    with open(path, "r") as file:
        arrival_time = int(file.readline())
        bus_lines = [int(i) for i in file.readline().split(",")
                        if i != "x"]
    file.close()
    return arrival_time, bus_lines

def EarliestDeparture(arrival_time:int, bus_lines:list[int]):
    # calculate the earliest departure for all bus lines after ferry arrival
    bus_index = 0
    bus_wait_times = [0]*len(bus_lines)
    for bus in bus_lines:
        bus_timer = 0
        while bus_timer < arrival_time:
            bus_timer += bus
        bus_wait_times[bus_index] = bus_timer
        bus_index += 1

    # linear search through bus lines to find the earliest departure
    bus_ID = bus_lines[0]
    min_wait_time = bus_wait_times[0] - arrival_time

    for index in range(1, len(bus_wait_times[1:])):
        wait_time = bus_wait_times[index] - arrival_time
        if (wait_time < min_wait_time):
            min_wait_time = wait_time
            bus_ID = bus_lines[index]

    return bus_ID, min_wait_time


def main():

    path = "data/data13.txt"
    arrival_time, bus_lines = LoadData(path)
    bus_ID, wait_time = EarliestDeparture(arrival_time, bus_lines)
    part_1 = bus_ID*wait_time

    print("\nDay 13 : Shuttle Search\n")
    print()
    print("--- part 1 ---")
    print("earliest bus line and wait time product =", part_1)
    print()
    print("--- part 2 ---")
    print("test")
    print()


if __name__ == "__main__":
    main()