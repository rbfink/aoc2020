import numpy as np

print()
print("DAY 01 : Report Repair")
print()

# read data
input = np.loadtxt("data/data01.txt", dtype=int)

reportLength = len(input)
for i in range(0,reportLength):
    for j in range(i,reportLength):
        A = input[i]
        B = input[j]
        if (A+B == 2020): # check for A+B=2020
            print("--- part 1 ---")
            print("A+B=2020 -> A*B =",A*B)
        for l in range(j,reportLength):
            C = input[l]
            if (A+B+C == 2020): # check for A+B+C=2020
                print()
                print("--- part 2 ---")
                print("A+B+C=2020 -> A*B*C =",A*B*C)
                print()
