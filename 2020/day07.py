import numpy as np
from lib.utils import *

# load data
filename = "data/data07.txt"
bagRules = LoadBagRules(filename)
# generate dictionary with all rules as key-value pairs
# key : bag colours as string ie. "shiny coral"
# value : list containing {"key":[[X,""],[Y,""]]
bagDict = GenerateBagDictionary(bagRules)

bagTarget = "shiny gold"
bagAtLeastOnceCount = BagCounter(bagRulesDict=bagDict,
                                 bagTargetKey=bagTarget,
                                 countNestedBags=False)

allNestedBags = BagCounter(bagRulesDict=bagDict,
                           bagTargetKey=bagTarget,
                           countNestedBags=True)

print("\nDay 07 : Handy Haversacks\n")
print()
print("--- part 1 ---")
print("amount of bag colors that contains at least one shiny gold bag =", bagAtLeastOnceCount)
print()
print("--- part 2 ---")
print("amount of nested bags inside a 'shiny gold' bag =", allNestedBags)
print()