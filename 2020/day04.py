import numpy as np
from lib.utils import DataNewlineSeperated2List, Passport2Dict, CheckPassport


# parse data so respective passport data is on one line
passportList = DataNewlineSeperated2List(filename="data/data04.txt")

validPassports = 0
validPassportsValidated = 0
for passport in passportList:
    passportDict = Passport2Dict(passport)
    validPassports += CheckPassport(passportDict,validation=False)
    validPassportsValidated += CheckPassport(passportDict,validation=True)

print("\nDAY 04 : Passport Procesing\n")
print("--- part 1 ---")
print("valid passports =", validPassports)
print()
print("--- part 2 ---")
print("valid passports after validation =", validPassportsValidated)
print()
