from lib.utils import SeatingSystem


path = "data/data11.txt"
seatingSystem = SeatingSystem(filename=path)
seatingSystem.FillSeats(neighborLimit=4)
filledSeatAdjacent = seatingSystem.filledSeats

seatingSystem.FillSeats(neighborLimit=5, LineOfSight=True)
filledSeatLineOfSight = seatingSystem.filledSeats

print("\nDay 11 : Seating System\n")
print()
print("--- part 1 ---")
print("filled seats when counting adjacent neighbors =", filledSeatAdjacent)
print()
print("--- part 2 ---")
print("filled seats when counting Line of Sight neighbors =", filledSeatLineOfSight)
print()