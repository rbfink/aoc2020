def NewTurn(game_dictionary: dict) -> None:
    for k in game_dictionary:
        game_dictionary[k] += 1

def GameLogic(number: int,
              turn_count: int,
              number_age: dict) -> None:
    # number has been spoken before
    if (number in number_age.keys()):
        new_number = turn_count - number_age[number]
        number_age[number] = 0
        print(f"turn {turn_count} number = {number}, new number = {new_number}")            
        number = new_number

    # number has not been spoken before
    else:
        print(f"turn {turn_count} number = {number}, new number = {0}")
        number_age[number] = 0
        number = 0

def MemoryGame(starting_numbers: list[int], play_count: int) -> int:
    start_numbers_len = len(starting_numbers)
    # dict containing all spoken numbers and the time since they where last spoken
    number_age = {}
    turn_count = 0
    number = starting_numbers[-1]

    # initialize memory game with the starting numbers
    for i in range(start_numbers_len):
        turn_count += 1
        GameLogic(starting_numbers[i], turn_count,number_age)
        # print(f"turn {turn_count} starting number = {starting_numbers[i]}")

    while (turn_count < play_count):
        turn_count += 1
        GameLogic(number, turn_count, number_age)

    print(f"{play_count}th number = {number} with starting numbers: {starting_numbers}")

    return number

def main() -> None:
    path = "data/data15.txt"
    
    last_round = 10
    test_data = [[1, 3, 2],
                 [2, 1, 3],
                 [1, 2, 3],
                 [2, 3, 1],
                 [3, 2, 1],
                 [3, 1, 2]]
    
    # for start_numbers in test_data:
    #     MemoryGame(start_numbers, last_round)
    MemoryGame([0, 3, 6], last_round)
    
    # printing
    print("\nDay 15 : Rambunctious Recitation\n")
    print()
    print("--- part 1 ---")
    print("part1 = ", "placeholder")
    print()
    print("--- part 2 ---")
    print("part2 = ", "placeholder")
    print()

if __name__ == "__main__":
    main()