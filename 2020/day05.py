import numpy as np
from lib.utils import BoardingPassInterpreter

# load data
boardingPassArray = np.genfromtxt('data/data05.txt',dtype=str)

# find the max seatID
seatID_List = []
for boardingPass in boardingPassArray:
    seatID_List.append(BoardingPassInterpreter(boardingPass)[2]) # only need seatID
seatIDmax = max(seatID_List)

# find your own seat
seatID_List.sort()
for i in range(len(seatID_List)-1):
    if (seatID_List[i]+1 != seatID_List[i+1]):
        mySeatID = seatID_List[i]+1
        break

print("\nDay 05 : Binary Boarding\n")
print("--- part 1 ---")
print("maximum seatID on a boarding pass =", seatIDmax)
print()
print("--- part 2 ---")
print("my seat ID =", mySeatID)
print()