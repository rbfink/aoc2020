
import numpy as np


def ParseMask(maskLine: str) -> list[str]:
    mask = list(maskLine[7:])
    return mask

def ParseMemoryAdress(memLine: str) -> tuple[int, int]:
    adressAndValue: list[str] = memLine.split("[")[1].split("] = ")
    adress = int(adressAndValue[0])
    value = int(adressAndValue[1])
    return adress, value

def FerryBinaryInit(dataPath:str) -> int:
    memorybank: dict = {}
    memAdress: int = 0
    mask: list[str] = ["X"]*36
    bitList: list[str] = ["0"]*36

    # open file and read first line
    dataFile = open(dataPath, "r")
    currentLine = dataFile.readline().rstrip('\n')

    # loop through masks and data
    while (currentLine):

        # load mask
        if (currentLine[0:4] == "mask"):
            mask = ParseMask(currentLine)
            currentLine = dataFile.readline().rstrip('\n')
            continue
        
        # parse adress and value
        memAdress, value = ParseMemoryAdress(currentLine)
        bitValue = format(value, "b")
        bitList = list("0"*(36-len(bitValue)) + bitValue)

        # filter value through mask before writing to memory
        for i in range(len(mask)):
            if (mask[i] != "X"):
                bitList[i] = mask[i]

        # write to memory..
        memorybank[memAdress] = int("".join(bitList), 2)

        # read next line
        currentLine = dataFile.readline().rstrip('\n')
  
    # calcuate sum of all values in memory
    valueReturnSum = sum(list(memorybank.values()))
    return valueReturnSum



def main():

    path = "data/data14test.txt"
    part1 = FerryBinaryInit(path)

    # printing
    print("\nDay 14 : Docking Data\n")
    print()
    print("--- part 1 ---")
    print("sum of values still in memory = ", part1)
    print()
    print("--- part 2 ---")
    print("placeholder")
    print()


if __name__ == "__main__":
    main()