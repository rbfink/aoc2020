from lib.utils import JoltageAdapter

filepath = "data/data10.txt"
joltageAdapter = JoltageAdapter(filename=filepath)
joltageAdapter.Load()
joltageAdapter.GenerateSortedAdapterChain()
joltageAdapter.GenerateValidPermutations()


part1_product = joltageAdapter.joltageDifferences[0] * joltageAdapter.joltageDifferences[2]
print("\nDay 10 : Adapter Array\n")
print()
print("--- part 1 ---")
print("product of of the sums of 1-jolt and 3-jolt differences =", part1_product)
print()
print("--- part 2 ---")
print("All unique way to sequence the joltage adapters =", joltageAdapter.possiblePermutations)
print()