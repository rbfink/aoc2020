import numpy as np
import copy
import re
from lib.math import MultiplyList

# Day 2: Password Philosophy

def CheckPasswordSled(passwordCandidate):
    # parse password data
    letter, rangeStart, rangeEnd, password = PasswordLetterAndRange(passwordCandidate)
    # take sum of letter occurences in password
    letterSum = np.char.count(password, letter)
    if (letterSum>=rangeStart 
            and letterSum<=rangeEnd):
        return 1
    else:
        return 0

def CheckPasswordToboggan(passwordCandidate):
    letter, rangeStart, rangeEnd, password = PasswordLetterAndRange(passwordCandidate)
    if (letter==password[rangeStart-1] 
            and letter==password[rangeEnd-1]):
        return 0
    elif (letter==password[rangeStart-1] 
            or letter==password[rangeEnd-1]):
        return 1
    else:
        return 0

def PasswordLetterAndRange(passwordCandidate):
    """
    Parse the password data from line read in datafile.

    Input : 
        str formatted 'rangeStart-rangeEnd letter: password'
    Output : 
        str letter, int rangeStart, int rangeEnd, str password
    """
    passwordSplit = passwordCandidate.split(": ")
    password = passwordSplit[1]
    passwordSplit = passwordSplit[0].split(" ")
    letter = passwordSplit[1]
    passwordSplit = passwordSplit[0].split("-")
    rangeStart = int(passwordSplit[0])
    rangeEnd = int(passwordSplit[1])
    return letter, rangeStart, rangeEnd, password

# Day 3: Toboggan Trajectory

def SlopeRiderTreeCounter(
        slopeArray, hStart, hStep, vStep,
        noTree=".", tree="#"):
    #slopeArray = SlopeArrayGenerator(slopeLines)
    treeCount = 0
    hPos = hStart
    vPos = 0
    # start traversing slope (modulo to prevent going out of range)
    while (vPos < slopeArray.shape[0]):
        # check position
        if (slopeArray[vPos][hPos] == noTree):
            pass
        elif (slopeArray[vPos][hPos] == tree):
            treeCount += 1
        elif (slopeArray[vPos][hPos] != noTree 
                and slopeArray[vPos][hPos] != tree):
            print("no readable reached in pos [{vPos}][{hPos}]")
            break
        # take step
        vPos += vStep
        hPos += hStep
        if (hPos >= slopeArray.shape[1]): # account for overstepping
            hPos = hPos-slopeArray.shape[1]
    return treeCount

def SlopeArrayGenerator(slopeLines):
    # generate numpy array and fill out
    slopes = len(slopeLines)
    slopeWidth = len(slopeLines[0])
    slopeArray = np.empty((slopes,slopeWidth), dtype=str)
    for slopeline in range(0,slopes):
        for element in range(0,slopeWidth):
            slopeArray[slopeline,element] = slopeLines[slopeline][element]
    return slopeArray

# Day 4: Passport Processing

def CheckPassport(
        passportDict,
        mustHaveList=['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'],
        canHaveList= ['cid'],
        validation=True):
    mustHaveCheck = MultiplyList([mustHave in passportDict 
                                  for mustHave in mustHaveList])
    canHaveCheck = MultiplyList([canHave in passportDict 
                                 for canHave in canHaveList])

    passportValid = False
    # check if passport is valid without validation
    if (mustHaveCheck 
            and len(passportDict)==len(mustHaveList)):
        passportValid = True
    elif (mustHaveCheck 
            and canHaveCheck
            and len(passportDict)-len(mustHaveList)<=len(canHaveList)):
        passportValid = True

    # If validation is flagged, do it
    if (passportValid and validation):
        # validation data
        byr = range(1920, 2002+1)
        iyr = range(2010, 2020+1)
        eyr = range(2020, 2030+1)
        hgt = [['cm', 'in'], 
               [range(150, 193+1), range(59, 76+1)]]
        hcl = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
        ecl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
        pid = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        cid = True # always ignored
        
        # run validation of passport entries
        byrValid = (int(passportDict['byr']) in byr)
        iyrValid = (int(passportDict['iyr']) in iyr)
        eyrValid = (int(passportDict['eyr']) in eyr)
        # hgt check        
        if (passportDict['hgt'][-2:] == 'cm' 
                and int(passportDict['hgt'][:-2]) in hgt[1][0]):
            hgtValid = True
        elif(passportDict['hgt'][-2:] == 'in' 
                and int(passportDict['hgt'][:-2]) in hgt[1][1]):
            hgtValid = True
        else:
            hgtValid = False
        # print("hcl =",passportDict['hcl'])
        hclValid = (passportDict['hcl'][0] == '#'
                    and (MultiplyList(entry in hcl for entry in passportDict['hcl'][1:])))
        eclValid = (passportDict['ecl'] in ecl)
        pidValid = (len(passportDict['pid']) == 9
                    and MultiplyList([number in pid for number in passportDict['pid']]))
        cidValid = cid
        if (byrValid
                and iyrValid
                and eyrValid
                and hgtValid
                and hclValid
                and eclValid
                and pidValid
                and cidValid):
            passportValid = True
        else:
            passportValid = False
    return passportValid

def Passport2Dict(passportString, entrySeperator=" ", keySeperator=":"):
    """
    takes day04 passport formatted in one string and generates a dict
    of the key-value pairs of the passport.
    """
    # split the long string into key-value pairs
    passportList = passportString.split(entrySeperator)
    # split key-value pairs into sublists
    passportList = [entry.split(keySeperator) for entry in passportList]
    # generate dictionary with key-value pairs
    passportDict = {entry[0]: entry[1] for entry in passportList}
    return passportDict

def DataNewlineSeperated2List(filename:str, spaceSeperation:bool=True):
    """
    Reads unstructured data file from day04 and returns a python list with every
    line containing all data entries per passport seperated by space
    """
    seperator = ":nextpassport:"
    # read and parse data
    dataFile = open(filename, "r")
    passportInput = dataFile.read().replace("\n\n",seperator)
    dataFile.close()
    if (spaceSeperation):
        passportInput = passportInput.replace("\n"," ")
    else:
        passportInput = passportInput.replace("\n","")
    passportList = passportInput.split(seperator)
    return passportList

# day 5: Binary Boarding

def BoardingPassInterpreter(boardingPass):
    rows = [i for i in range(0, 128)]
    columns = [i for i in range(0, 8)]

    # find the row and column
    for letter in boardingPass:
        if (letter=='F'):
            rows = rows[:len(rows)//2]
        elif (letter=='B'):
            rows = rows[len(rows)//2:]
        elif (letter=='L'):
            columns = columns[:len(columns)//2]
        elif (letter=='R'):
            columns = columns[len(columns)//2:]
    
    row = rows[0]
    column = columns[0]
    # generate seatID
    seatID = row*8 + column
    return [row, column, seatID]

# day 6 : Custom Customs

def RemoveDuplicatesFromStringUnordered(inputString:str):
    """
    Creates set(inputString) which is unordered and joins it 
    with an empty string with no insertion in between

    return "".join(set(inputString))
    """
    return "".join(set(inputString))

def FindCommonAnswersSpaceSeperated(inputString:str):
    # find number of members in group
    groupCount = inputString.count(" ") + 1
    # remove space seperation
    groupAnswers = inputString.replace(" ","")
    # find questions where all group members answered yes
    commonAnswersAmount = 0
    commonAnswersList = []
    for answer in groupAnswers:
        if (groupAnswers.count(answer)==groupCount 
                and answer not in commonAnswersList):
            commonAnswersList.append(answer)
            commonAnswersAmount += 1
    return [commonAnswersList, commonAnswersAmount]

# day 7 : Handy haversacks

def LoadBagRules(filename:str):
    """
    Reads the raw puzzle input for day07 part 1 and returns each rule
    as a long string 
    
    input : filename:str
    - datafile relative path
    
    
    output : bagRules:list<str>
    - each entry in list contains a bag rule as a continous string
    """
    seperator = ":newrule:"
    # open file as file object in readmode
    fileReader = open(filename, "r")
    bagRulesRaw = fileReader.read().replace("\n", seperator)
    fileReader.close()
    # split long string to seperate rules into list
    bagRules = bagRulesRaw.split(seperator)
    return bagRules

def GenerateBagDictionary(bagRules):
    # generate Dict with bag name rule
    ruleRange = range(len(bagRules))
    bagRulesDict = {}
    for rule in ruleRange:
        # remove trailing " bags." or " bag."
        if bagRules[rule][-2] == "s":
            bagRuleNoTrail = bagRules[rule][:-6]
        else:
            bagRuleNoTrail = bagRules[rule][:-5]
        # split long string into mother bag and nested bags
        ruleSplit = re.split(" bags contain | bags, | bag, ", bagRuleNoTrail)
        ruleKey = ruleSplit[0]
        # parse nested bags into nested list [[int, bagname], ...]
        ruleValueList = []
        for value in ruleSplit[1:]:
            valueSplit = value.split(" ", 1)
            if (valueSplit[0] == "no"):
                valueSplit[0] = 0
            else:
                valueSplit[0] = int(valueSplit[0])
            ruleValueList.append(valueSplit)
        # insert key-value pair into dictionary
        bagRulesDict[ruleKey] = ruleValueList
    return bagRulesDict

def BagCounter(bagRulesDict:dict, bagTargetKey:str, countNestedBags=False):
    if (countNestedBags):
        nestedBagCount = NestedBagCounter(bagRulesDict, bagTargetKey)
        # discount the initial nestedBagsCount = 1, and return
        return nestedBagCount-1
    else:
        # how many bags eventually contain bagTargetKey at least once
        bagAtLeastOnceCount = 0
        bagKeyList = list(bagRulesDict)
        for bagKey in bagKeyList:
            # call recursive BagCounter function
            bagResults = BagTargetCounter(bagRulesDict, bagKey, bagTargetKey)
            if (bagResults>0):
                bagAtLeastOnceCount += 1
        return bagAtLeastOnceCount

def BagTargetCounter(bagRulesDict:dict, bagKey:str, bagTargetKey:str):
    bagCount = 0
    for bagAmount, bagName in bagRulesDict[bagKey]:
        if (bagAmount and bagTargetKey==bagName):
            bagCount += bagAmount
        elif (bagAmount):
            bagCount += bagAmount*BagTargetCounter(bagRulesDict,
                                                   bagName,
                                                   bagTargetKey)
    return bagCount

def NestedBagCounter(bagRulesDict:dict, bagKey:str):
    nestedBagsCount = 1
    for bagAmount, bagName in bagRulesDict[bagKey]:
        if (bagAmount):
            nestedBagsCount += bagAmount*NestedBagCounter(bagRulesDict, bagName)
    return nestedBagsCount

# day 8 : Handheld Halting

class BootCode:

    def __init__(self, filename:str):
        self.filename = filename
        self.bootCode = []
        self.bootCodeBugged = bool
        self.failedAccumulatorValue = None
        self.successAccumulatorValue = None

    def Load(self):
        fileReader = open(self.filename)
        bootCodeRaw = fileReader.read().split("\n")
        fileReader.close
        instructionAmount = len(bootCodeRaw)
        self.bootCode = [[]]*instructionAmount
        for i in range(instructionAmount):
            self.bootCode[i] = bootCodeRaw[i].split(" ")
            self.bootCode[i][1] = int(self.bootCode[i][1])
        self.bootCodeLength = len(self.bootCode)

    def RunBootCode(self, bootCode2Run:list=None, Print=False):
        if (bootCode2Run == None):
            bootCode2Run = self.bootCode.copy()
        pointer = 0
        accumulator = 0
        operations = 0
        debugList = [0]*self.bootCodeLength
        # boot code can at most run all instructions once
        while (operations<self.bootCodeLength):
            operations += 1
            # check if bootcode is successfull
            if (pointer == self.bootCodeLength):
                self.successAccumulatorValue = accumulator
                self.bootCodeBugged = False
                if (Print):
                    print("BootCode ran successfully")
                return True
            instruction = bootCode2Run[pointer][0]
            value = bootCode2Run[pointer][1]
            # check if instruction has been run, no repeats are allowed
            if (debugList[pointer] > 0 
                    or pointer > self.bootCodeLength):
                self.failedAccumulatorValue = accumulator
                self.bootCodeBugged = True
                if (Print):
                    print("BootCode malfuntioned.")
                return False
            # if not a repeat, continue with bootcode
            elif (instruction=="nop"):
                # print(f"pointer = {pointer} -> nop")
                debugList[pointer] += 1
                pointer += 1
            elif (instruction=="jmp"):
                # print(f"pointer = {pointer} -> jmp")
                debugList[pointer] += 1
                pointer += value
            elif (instruction=="acc"):
                # print(f"pointer = {pointer} -> acc")
                debugList[pointer] += 1
                accumulator += value
                pointer += 1

    def FixBootCode(self):
        # make ready for brute force debug, finding all possible bug locations
        if (self.bootCodeBugged):
            bugIndex = 0
            potentialBugs = []
            for i in range(self.bootCodeLength):
                if (self.bootCode[i][0] == "jmp"
                        or self.bootCode[i][0] == "nop"):
                    potentialBugs.append(i)
        # Check all possible bug instruction changing one 'nop' -> 'jmp' or vice versa
        while (self.bootCodeBugged and bugIndex<len(potentialBugs)):
            # reset bootcode, as only one entry is changed for a bug fix
            bootCodeDebugging = copy.deepcopy(self.bootCode)
            # check the next possible bug
            bugCheck = potentialBugs[bugIndex]
            # increase bug indexer if this debug fails
            bugIndex += 1
            if (self.bootCode[bugCheck][0]=="jmp"):
                bootCodeDebugging[bugCheck][0] = "nop"
                if(self.RunBootCode(bootCodeDebugging)):
                    self.bootCodeFixed = bootCodeDebugging.copy()
            elif (self.bootCode[bugCheck][0]=="nop"):
                bootCodeDebugging[bugCheck][0] = "jmp"
                if (self.RunBootCode(bootCodeDebugging)):
                    self.bootCodeFixed = bootCodeDebugging.copy()
            else:
                print("FixBootCode internal method failed...")
                break

# day 9 : Encoding Error

class DecoderXMAS:

    def __init__(self, filename:str, preambleDepth:int=25) -> None:
        self.filename = filename
        self.preambleDepth = preambleDepth
        self.nonComformingValue = 0

    def Load(self):
        self.data = np.genfromtxt(self.filename, dtype=np.int64)
        self.dataLength = len(self.data)

    def CheckData(self):
        # starting position
        pointer = self.preambleDepth
        depth = self.preambleDepth
        while(pointer<self.dataLength):
            if (self.CheckNumber(pointer)):
                pointer += 1
            else:
                self.nonComformingValue = self.data[pointer]
                pointer += 1
            
    def CheckNumber(self, pointer:int):
        depth = self.preambleDepth
        target = self.data[pointer]
        targetSuccess = False
        for i in self.data[pointer-self.preambleDepth:pointer]:
            if (targetSuccess): # not the best solution, should refactor
                break
            for j in self.data[pointer-self.preambleDepth+1:pointer]:
                if (i+j == target):
                    targetSuccess = True
                    break
        return targetSuccess

    def FindContigousSet(self):
        if (self.nonComformingValue == 0):
            self.CheckData()
        pointer = 0
        weaknessNotFound = True
        while (weaknessNotFound and pointer<len(self.data)):
            summ = 0
            start = pointer
            for i in self.data[pointer:]:
                summ += i
                if (summ == self.nonComformingValue):
                    end = np.where(self.data == i)[0][0]
                    weaknessNotFound = False
                    break
                elif (summ > self.nonComformingValue):
                    pointer += 1
                    break
        self.contigousSet = self.data[start:end]

# day 10 : Adapter Array

class JoltageAdapter:
    
    def __init__(self, filename:str, outletJoltage:int=0):
        self.filename = filename
        self.outletJoltage = outletJoltage

    def Load(self):
        """
        Made the loading in raw python3 after inspiration from the 
        AOC subreddit...
        """
        with open(self.filename, "r") as data:
            self.joltageAdapters = [int(line.rstrip()) for line in data]
        data.close()
        self.deviceAdapterJoltage = max(self.joltageAdapters) + 3
        self.sortedAdapters = sorted(self.joltageAdapters)
        self.completeSorted = [self.outletJoltage]+self.sortedAdapters+[self.deviceAdapterJoltage]
        self.completeLength = len(self.completeSorted)

    def GenerateSortedAdapterChain(self):
        # array containing amount of 1,2 and 3 joltage jumps in adapter chain
        self.joltageDifferences = np.zeros(3, np.int64)
        # count all joltage differences between adapters
        for i in range(self.completeLength-1):
            joltageDifference = self.completeSorted[i+1] - self.completeSorted[i]
            self.joltageDifferences[joltageDifference - 1] += 1

    def GenerateValidPermutations(self, startIndex:int=0):
        counter = {0: 1}
        for adapter in self.completeSorted[1:]:
            counter[adapter] = counter.get(adapter-3, 0) + counter.get(adapter-2, 0) + counter.get(adapter-1, 0)
        self.possiblePermutations = counter[self.completeSorted[-1]]

# day 11 : Seating System

class SeatingSystem:

    def __init__(self, filename:str) -> None:
        self.filename = filename
        with open(self.filename,"r") as data:
            self.seatingStart = [list(row.rstrip()) for row in data]
        data.close()
        self.rowMax = len(self.seatingStart)
        self.colMax = len(self.seatingStart[0])

    def FillSeats(self, neighborLimit,  LineOfSight=False):
        gridIn = copy.deepcopy(self.seatingStart)
        self.changes = 1
        while(self.changes):
            self.changes = 0
            gridOut = self.__FillSeats(gridIn, 
                                       neighborLimit,
                                       LineOfSight)
            
            if (self.changes):
                gridIn = gridOut

            else:
                self.seatingStable = gridIn
                self.filledSeats = sum([row.count("#") for row in self.seatingStable])

    def __FillSeats(self, grid, neighborLimit, LineOfSight=False):
        gridOut = copy.deepcopy(grid)
        for row in range(self.rowMax):
            for col in range(self.colMax):

                if (LineOfSight):
                    neighbors = self.__LineOfSight(grid, row, col)
                else:
                    neighbors = self.__Adjacent(grid, row, col)

                if (grid[row][col] == "L" and neighbors == 0):
                    gridOut[row][col] = "#"
                    self.changes += 1
                elif (grid[row][col] == "#" and neighbors >= neighborLimit):
                    gridOut[row][col] = "L"
                    self.changes += 1
                else:
                    gridOut[row][col] = grid[row][col]

        return gridOut

    def __Adjacent(self, grid, row, col):
        neighbors = 0
        for r in (-1, 0, 1):
            for c in (-1, 0, 1):

                if (r == c == 0):
                    continue

                elif (0<= row+r < self.rowMax 
                          and 0<= col+c < self.colMax
                          and grid[row+r][col+c] == "#"):
                    neighbors += 1

        return neighbors

    def __LineOfSight(self, grid, row, col):
        # directions = ["n", "ne", "e", "se", "s", "sw", "w", "nw"]
        directions = [[-1, 0], [-1, 1], [0, 1], [1, 1], 
                      [1, 0], [1, -1], [0, -1], [-1, -1]]
        neighbors = 0
        for r, c in directions:
            radius = 1
            while (0 <= row + r*radius < self.rowMax 
                       and 0 <= col + c*radius < self.colMax):

                if (grid[row + r*radius][col + c*radius] == "#"):
                    neighbors += 1
                    break

                elif (grid[row + r*radius][col + c*radius] == "L"):
                    break
                
                else:
                    radius += 1
                    continue
        
        return neighbors
    
