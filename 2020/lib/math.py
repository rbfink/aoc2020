import numpy as np


def MultiplyList(inputList):
    product = 1
    for entry in inputList:
        product = product * entry
    return product